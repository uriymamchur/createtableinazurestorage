﻿$storageAccountResourceGroup="test-env"
$tableName="userdatainfo"
$storageAccountName = "sa$tableName"

$SaContext = (Get-AzStorageAccount -ResourceGroupName $storageAccountResourceGroup -Name $storageAccountName).Context
New-AzStorageTable -Name $tableName -Context $saContext
$table = (Get-AzStorageTable -Name $tableName -Context $saContext).CloudTable
$partitionKey1 = "User 1"
$partitionKey2 = "User 2"
$partitionKey3 = "User 3"
Add-AzTableRow `
    -table $table `
    -partitionKey $partitionKey1 `
    -rowKey ("TestPJ") -property @{"fullUserName"="User 1";"shortUserName"="user1";"userId"="kk85498hdhfsd954890320948";"isActivated"="true"}
Add-AzTableRow `
    -table $table `
    -partitionKey $partitionKey2 `
    -rowKey ("TestPJ") -property @{"fullUserName"="User 2";"shortUserName"="user2";"userId"="kjnkjfd909043590jdgjf043959083";"isActivated"="true"}
Add-AzTableRow `
    -table $table `
    -partitionKey $partitionKey3 `
    -rowKey ("TestPJ") -property @{"fullUserName"="Test User 3";"shortUserName"="testuser3";"userId"="04385949fdhkjgh9438889045hvckj";"isActivated"="false"}

